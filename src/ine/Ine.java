package ine;

public class Ine extends Persona {
	private String claveElector;
	private String estado;
	private String localidad;
	private String municipio;
	private String anoRegistro;
	private String seccion;
	private String emision;
	private String vigencia;

	public Ine() {}
	
	public Ine(String nombres, String apellidoP, String apellidoM, String domicilio, String curp, String sexo,
			String fechaNac, String claveElector, String estado, String localidad, String municipio, String anoRegistro,
			String seccion, String emision, String vigencia) {
		super();
		setNombres(nombres.toUpperCase());
		setApellidoP(apellidoP.toUpperCase());
		setApellidoM(apellidoM.toUpperCase());
		setDomicilio(domicilio.toUpperCase());
		setCurp(curp.toUpperCase());
		setSexo(sexo.toUpperCase());
		setFechaNac(fechaNac);
		this.claveElector = claveElector.toUpperCase();
		this.estado = estado;
		this.localidad = localidad;
		this.municipio = municipio;
		this.anoRegistro = anoRegistro;
		this.seccion = seccion;
		this.emision = emision;
		this.vigencia = vigencia;
	}

	public String getClaveElector() {
		return claveElector;
	}

	public void setClaveElector(String claveElector) {
		this.claveElector = claveElector;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getAnoRegistro() {
		return anoRegistro;
	}

	public void setAnoRegistro(String anoRegistro) {
		this.anoRegistro = anoRegistro;
	}

	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public String getEmision() {
		return emision;
	}

	public void setEmision(String emision) {
		this.emision = emision;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
}
