package ine;

import java.util.ArrayList;
import java.util.List;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

public class Registro {

	private ObjectContainer db = null;

	private void abrirRegistro() {
		db = Db4oEmbedded.openFile("ine.db4o");
	}

	public void insertarRegistro(Ine i) {
		// TODO Auto-generated method stub
		abrirRegistro();
		db.store(i);
		cerrarRegistro();
	}
	
	public List<Ine> seleccionarIne(){
		abrirRegistro();
		ObjectSet listaIne = db.queryByExample(Ine.class);
		List<Ine> li= new ArrayList<>();
		
		for (Object listaIne1 : listaIne) {
			li.add((Ine) listaIne1);
		}
		cerrarRegistro();
		return li;
	}

	private void cerrarRegistro() {
		db.close();
	}
}
